<?php
   
namespace app\models;

use Yii;
use yii\base\Model;

class RegFormClass26 extends Model
{
    public $username;
    public $password;
    public $email;
    public $country;
    public $city;
    public $phone;
    public function rules()
    {
        return [
           ['city', 'validateCity']
        ];
    }
    public function validateCity($attribute, $params)
    {
        if (!in_array($this->$attribute, ['Paris', 'London'])) {
            $this->addError($attribute, 'The city must be either "London" or "Paris".');
        }
    }
}
