<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\components\CityValidator;

class RegFormClass262 extends Model
{
    public $username;
    public $password;
    public $email;
    public $country;
    public $city;
    public $phone;
    public function rules()
    {
        return [
           ['city', CityValidator::className()]
        ];
    }
}
