<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
         ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'n1anXFbsmI2QcDJyD4SGswQtD9RxIUYi',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
             ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
            'maxSourceLines' => 10,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'flushInterval' => 1,
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'exportInterval' => 1,
                    'logVars' => []
                ],
            ],
        ],
        'view' => [
            'theme' => [
               'basePath' => '@app/themes/basic',
               'baseUrl' => '@web/themes/basic',
               'pathMap' => [
                  '@app/views' => [
                     '@app/themes/christmas',
                     '@app/themes/basic',
                  ],
               ]
            ],
         ],
        'db' => $db,
        'cache' => [
            'class' => 'yii\caching\FileCache',
         ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            // 'suffix' => '.fgl',
            'rules' => [
               ['class' => 'yii\rest\UrlRule', 'controller' => 'RestUser'],
               'about' => 'site/about',
            ],
         ],
         'i18n' => [
            'translations' => [
               'app*' => [
                  'class' => 'yii\i18n\PhpMessageSource',
                  'fileMap' => [
                     'app' => 'app.php'
                  ],
               ],
            ],
         ],
    ],
    'modules' => [
        'hello' => [
           'class' => 'app\modules\hello\Hello',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
         ],
     ],
    //changing the default controller
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
