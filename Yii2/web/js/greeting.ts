class Greeter {
    constructor(public greeting: string) { }
    greet() {
       return this.greeting;
    }
 };
 var greeter = new Greeter('Class 16 - Hello from typescript!');
 var html = document.querySelector(".TSDemo");

 if (html){
    html.addEventListener("click", function(){
        alert(greeter.greet());
     });
 }


