var Greeter = /** @class */ (function () {
    function Greeter(greeting) {
        this.greeting = greeting;
    }
    Greeter.prototype.greet = function () {
        return this.greeting;
    };
    return Greeter;
}());
;
var greeter = new Greeter('Class 16 - Hello from typescript!');
var html = document.querySelector(".TSDemo");
if (html) {
    html.addEventListener("click", function () {
        alert(greeter.greet());
    });
}
