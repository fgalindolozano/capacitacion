<?php 
    
    $this->title = 'Class 6 - Entry Scripts';
    
?>

<div class="row">
    <div class="col-md-6">
        <h3>Anotaciones de esta clase</h3>
        <p>El script de inicio es el eje central de las aplicaciones, ya que en el : </p>
        <ul>
            <li>Se definen las constantes globales</li>
            <li>Se carga el autoloader de Composer</li>
            <li>Se llaman los archivos vitales de Yii2</li>
            <li>Se carga la configuracion de la aplicacion</li>
            <li>Se instancia la aplicacion, y se le aplica la configuracion</li>
            <li>Se procesan las peticiones entrantes</li>
        </ul>
    </div>
    <div class="col-md-6">
        <h3>Como funciona el script de inicio?</h3>
        <p>Este diagrama muestra el comportamiento del script de inicio : </p> 
        <img src="https://www.tutorialspoint.com/yii/images/entry_scripts_structure.jpg">
    </div>
</div>