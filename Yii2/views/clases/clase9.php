<?php
    use yii\helpers\Url;

$this->title = 'Class 9 - Entry Scripts';
    
?>

<div class="row">
    <div class="col-md-4">
        <h3>Ejemplos de acciones</h3>
        <ul>
            <li><a href="<?= Url::to(['clases/test']) ?>">
                    Acciones independientes (dentro de componentes)</a></li>
            <li><a href="<?= Url::to(['clases/simple']) ?>?r=clases/simple">
                    Acciones con contenido sencillo</a></li>
            <li><a href="<?= Url::to(['clases/go-to-fb']) ?>?r=clases/go-to-fb">
                    Acciones para redireccionar</a></li>
            <li><a href="<?= Url::to(['clases/parameters','fs'=>'Hola','sc'=>'Mundo']) ?>">
                    Acciones con parametros</a></li>
        </ul>
    </div>
    <div class="col-md-8">
        <h3>Anotaciones de esta clase</h3>
        <p>Existen dos tipos de acciones que se pueden crear en Yii2 : acciones independientes o
            acciones de un controlador. Ambas se definen siguiendo los siguientes pasos: </p>
        <ul>
            <li>Volver mayusculas todas las primeras letras de su ID.</li>
            <li>Quitarle todos los guiones que contenga</li>
            <li>Colocarle el sufijo "action" al resultado</li>
        </ul>
        <p>Como se puede apreciar en el menu lateral, las acciones pueden llevar a cabo tareas
            diversas, pero el desarrollador deberia ceñirse a las siguientes recomendaciones: </p>
        <ul>
            <li>Cada accion deberia ser lo mas atomica posible, su escritura no debe ser compleja ni extensa</li>
            <li>Utilizar vistas como respuesta</li>
            <li>No deberia incluir codigo HTML</li>
            <li>Llamar metodos de los modelos</li>
            <li>No procesar informacion. Estos deben ser procesados en los modelos</li>
        </ul>
    </div>
</div>