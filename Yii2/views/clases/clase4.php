<?php 
    
    use yii\helpers\Html; 

    $this->title = 'Class 4 - Create Page';
    echo '<h3> #4: Create Page </h3>'; 
    echo '<b>Your message : </b>'.Html::encode($message);
?> 

<div class="row">
    <h3>Anotaciones de esta clase</h3>
    <p>Para crear una vista en Yii2 se deben manipular las siguientes cosas : </p> 
    <ol>
        <li>
            Se debe crear el archivo de la vista en un fichero PHP, que debera ser guardado en una carpeta
            (al parecer no es mandatario) que coincida con el nombre de su controlador.
            <br> <b>Nota: </b> Controlador se refiere a un objeto que se encuentra en la carpeta 
            {ROOT}/controllers, desde el cual se define el comportamiento de un contexto dentro del framework.
        </li>
        <li>
            Se debe definir una accion en el controlador que tendra la estructura action{Nombre}, (Nombre 
            representa como va a ser el URL de la vista)
            <br> <b>Nota: </b> Accion se refiere a una funcion desde la cual se define no solo el fichero que
            cargara al entrar a la URL que lleve su nombre, sino todos los aspectos adicionales que dicho fichero 
            requiera para su correcto funcionamiento (como variables, y probablemente otros recursos, etc.)
        </li>
        <li>
            Por ultimo, ingresar a la URL {ROOT}/?r={Modulo}/{Controlador}/{Accion}
        </li>
    </ol>
</div>