<?php 
    
    $this->title = 'Class 8 - Using Controllers';

    echo '<h3> #8: Using Controllers </h3>'; 
    echo '<b>Your message : </b>'.$message;
    
?>

<div class="row">
    <h3>Anotaciones de esta clase</h3>
    <p>Para nombrar un controlador, se deben seguir los siguientes pasos : </p> 
    <ul>
        <li>Volver mayusculas todas las primeras letras de su ID.</li>
        <li>Quitarle todos los guiones que contenga</li>
        <li>Reemplazar los slash por backlashes</li>   
        <li>Añadirle el sufijo "Controller"</li>
        <li>Colocarle el namespace al final del resultado</li>
    </ul>
</div>