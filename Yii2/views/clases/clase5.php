<?php 
    
    $this->title = 'Class 5 - Application Structure';
    
?>

<div class="row">
    <h3>Anotaciones de esta clase</h3>
    <p>Yii2 cuenta con un grupo de carpetas que clasifican recursos, las cuales son : </p> 
    <ol>
        <li><b>Assets: </b>Aqui se referencian todos los ficheros .js y .css que la aplicacion utilizara</li>
        <li><b>Commands: </b>Aqui se encuentran todos los controladores que pueden ser usados desde la terminal</li>
        <li><b>Config: </b>Aqui se encuentran los ficheros que contienen la configuracion de la aplicacion</li>
        <li><b>Mail: </b>Aqui se encuentran las plantillas de los correos electronicos</li>
        <li><b>Models: </b>Aqui se incluyen los ficheros de los modelos</li>
        <li><b>Runtime: </b>Aqui se incluye la informacion que se genera en tiempo de ejecucion (e.g. para debugging)</li>
        <li><b>Tests: </b>Aqui se almacenan los ficheros de prueba</li>
        <li><b>Vendor: </b>Aqui estaran todas las carpetas y ficheros que Composer administra</li>
        <li><b>Views: </b>En esta carpeta se posicionan no solo las vistas, sino la plantilla que todas seguiran</li>
        <li><b>Web: </b>Esta es la carpeta publica de la aplicacion, a la cual el usuario tendra acceso</li>
    </ol>
    <p>Ademas, Yii2 utiliza los siguientes objetos : </p> 
    <ol>
        <li><b>Models: </b> Representan la informacion de la capa de persistencia (e.g. PostreSQL)</li>
        <li><b>Views: </b> Muestra dicha informacion al usuario de manera transparente</li>
        <li><b>Controller: </b> Hacen de mediadores entre los dos objetos anteriormente mencionados</li>
        <li><b>Components: </b> Son objetos que contienen logica y pueden ser reutilizados posteriormente.</li>
        <li><b>App. components: </b>Son componentes que solo pueden ser instanciados una vez en toda la aplicacion</li>
        <li><b>Widgets: </b> Son componentes que no solo tienen logica sino que tambien generan una vista</li>
        <li><b>Filters: </b> Son objetos que se ejecutan tanto antes como despues de un controlador</li>
        <li><b>Modules: </b> Son basicamente miniaplicaciones, pues pueden contener todos los objetos anteriores</li>
        <li><b>Extensions: </b> Son los paquetes que pueden ser manejados por Composer</li>
    </ol>
</div>