<?php
   use yii\grid\GridView;

$this->title = 'Class 39 - GridView Widget';

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
       ['class' => 'yii\grid\SerialColumn'], 'name',
       ['class' => 'yii\grid\ActionColumn'],
       ['class' => 'yii\grid\CheckboxColumn'],
    ],
 ]);
