<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'Class 38 - ListView Widget';
?>
<div class="user">
    <?= $model->id ?>
    <?= Html::encode($model->name) ?>
    <?= HtmlPurifier::process($model->email) ?>
</div>