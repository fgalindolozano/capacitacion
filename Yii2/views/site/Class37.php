<?php
   use yii\widgets\DetailView;

   $this->title = 'Class 37 - Data Widgets';

echo DetailView::widget([
      'model' => $model,
      'attributes' => [
         'id',
         'name:html',
         [
            'label' => 'Correo',
            'value' => $model->email,
         ],
      ],
   ]);
