<?php
   /* @var $this yii\web\View */
   use yii\helpers\Html;
   use yii\helpers\HtmlPurifier;
   $this->title = 'Class 13 - Views';
   $this->params['breadcrumbs'][] = $this->title;
   $this->registerMetaTag(['name' => 'keywords', 
                           'content' => 'yii, developing, views, meta, tags']);
$this->registerMetaTag(['name' => 'description', 
                        'content' => 'This is the description of this page!'], 
                        'description');
?>
<div class="site-about">
   <h1>About</h1>
   <p>
      This is the About page. You may modify the following file to customize its content:
   </p>
   <h3> Encoding HTML (coming from user input) : </h3>
   <p>
      <?= Html::encode("<script>alert('alert!');</script><h1>ENCODE EXAMPLE</h1>>") ?>
   </p>
   <h3> Rendering partial HTML (coming from user input) : </h3>
   <p>
      <?= HtmlPurifier::process("<script>alert('alert!');</script><h1> HtmlPurifier EXAMPLE</h1>") ?>
   </p>
   
   <h3> Rendering views inside other views : </h3>
   <?= $this->render("_part1") ?>
   <?= $this->render("_part2") ?>

    <h3> Passing data to views (Read controllers/SiteController.php for more info) : </h3>
    <p>
        <b>email:</b> <?= $email ?>
    </p>
    <p>
        <b>phone:</b> <?= $phone ?>
    </p>

       <code><?= __FILE__ ?></code>
</div>