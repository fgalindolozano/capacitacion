<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Learning Yii 2 for TICMakers';

?>
<div class="site-index">
    <div class="body-content">
        <h3><b>EJERCICIOS Y ANOTACIONES DEL <a href="https://www.tutorialspoint.com/yii/index.htm">TUTORIAL DE YII</a></b></h3>
        <div class="col-lg-6">
            <div class="row">
                <ul>
                    <li><a href="<?= Url::to(['clases/class4', 'message'=>'Hello World']) ?>">
                            #4 - Crear una pagina</a></li>
                    <li><a href="<?= Url::to(['clases/class5']) ?>">
                            #5 - Estructura de una aplicacion</a></li>
                    <li><a href="<?= Url::to(['clases/class6']) ?>">
                            #6 - Script de inicio</a></li>
                    <li><a href="<?= Url::to(['clases/class8']) ?>">
                            #8 - Usando Controladores</a></li>
                    <li><a href="<?= Url::to(['clases/class9']) ?>">
                            #9 - Usando Acciones</a></li>
                    <li><a href="<?= Url::to(['site/show-cm']) ?>">
                            #10 - Modelos</a> /
                        <a href="<?= Url::to(['site/contact']) ?>">
                            Contacto</a></li>
                    <li><a href="<?= Url::to(['site/test-widget']) ?>">
                            #11 - Widgets</a></li>
                    <li><a href="<?= Url::to(['hello/custom/greet']) ?>">
                            #12 - Modulos</a></li>
                    <li><a href="<?= Url::to(['site/about']) ?>">
                            #13 - Vistas</a></li>
                    <li><a href="<?= Url::to(['clases/']) ?>">
                            #14 - Plantillas</a></li>
                    <li><a class="JSDemo" style='cursor: pointer;'>
                            #15 - Recursos</a></li>
                    <li><a class="TSDemo" style='cursor: pointer;'>
                            #16 - Conversion de recursos</a></li>
                    <li><a href="<?= Url::to(['clases/class17']) ?>">
                            #17 - Extensiones</a></li>
                    <li>
                        <a href="<?= Url::to(['site/request0',
                        ['id'=>'93', 'name'=>'TICMakers', 'message'=> 'Esto%20es%20un%20test']]) ?>">
                            #19 - Peticiones</a> /
                        <a href="<?= Url::to(['site/request1']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/request2',
                        ['name'=>'TICMakers', 'message'=> 'Test']]) ?>">
                            Ejercicio 3 </a> /
                        <a href="<?= Url::to(['site/request3']) ?>">
                            Ejercicio 4 </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['site/response0']) ?>">
                            #20 - Respuestas</a> /
                        <a href="<?= Url::to(['site/response1']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/response2']) ?>">
                            Ejercicio 3 </a> /
                        <a href="<?= Url::to(['site/response3']) ?>">
                            Ejercicio 4 </a> /
                        <a href="<?= Url::to(['site/response4']) ?>">
                            Ejercicio 5 </a> /
                        <a href="<?= Url::to(['site/response5']) ?>">
                            Ejercicio 6 </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['site/routes0']) ?>">
                            #22 - Enrutando URLs</a> /
                        <a href="<?= Url::to(['site/routes1']) ?>">
                            Ejercicio 2 </a>
                    </li>
                    <li><a href="<?= Url::to(['site/registration']) ?>">
                            #24 - Formularios HTML</a></li>
                    <li><a href="<?= Url::to(['site/validation']) ?>">
                            #25 - Validaciones</a></li>
                    <li>
                        <a href="<?= Url::to(['site/special-validation1']) ?>">
                            #26 - Validaciones especiales</a> /
                        <a href="<?= Url::to(['site/special-validation2']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/special-validation3']) ?>">
                            Ejercicio 3 </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['site/sesiones']) ?>">
                            #28 - Sesiones</a> /
                        <a href="<?= Url::to(['site/sesiones-datos']) ?>">
                            Colocar y acceder a datos de la sesion </a>
                    </li>
                    <li><a href="<?= Url::to(['site/flash-data']) ?>">
                            #29 - Datos Flash</a></li>
                    <li>
                        <a href="<?= Url::to(['site/send-cookies']) ?>">
                            #31 - Usando cookies</a> /
                        <a href="<?= Url::to(['site/read-cookies']) ?>">
                            Leyendo cookies</a>
                    </li>
                    <li><a href="<?= Url::to(['site/subir-archivos']) ?>">
                            #32 - Subiendo archivos</a></li>
                    <li><a href="<?= Url::to(['site/formateador']) ?>">
                            #33 - Formateador</a></li>
                    <li><a href="<?= Url::to(['site/paginacion']) ?>">
                            #34 - Paginacion de datos</a></li>
                    <li><a href="<?= Url::to(['site/sorting']) ?>">
                            #35 - Ordenamiento de datos</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
                <ul>
                    <li><a href="<?= Url::to(['site/objetos']) ?>">
                            #36 - Propiedades (manipulacion de objetos POO)</a></li>
                    <li>
                        <span>
                            #37 - Provedores de datos : </span>
                        <a href="<?= Url::to(['site/proveedores-de-datos1']) ?>">
                            Active DP</a> /
                        <a href="<?= Url::to(['site/proveedores-de-datos2']) ?>">
                            SQL DP</a> /
                        <a href="<?= Url::to(['site/proveedores-de-datos3']) ?>">
                            Array DP</a>
                    </li>
                    <li><a href="<?= Url::to(['site/data-widget']) ?>">
                            #38 - Widgets de datos</a></li>
                    <li><a href="<?= Url::to(['site/data-list-widget']) ?>">
                            #39 - Widget con listado de datos</a></li>
                    <li>
                        <a href="<?= Url::to(['site/data-grid-widget1']) ?>">
                            #40 - Widget con cuadricula de datos</a> /
                        <a href="<?= Url::to(['site/data-grid-widget2']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/data-grid-widget3']) ?>">
                            Ejercicio 3 </a>
                    </li>
                    <li><a href="<?= Url::to(['site/test-event']) ?>">
                            #42 - Creando eventos</a></li>
                    <li><a href="<?= Url::to(['site/test-behavior']) ?>">
                            #44 - Creando comportamientos</a></li>
                    <li><a href="<?= Url::to(['site/test-interface']) ?>">
                            #46 - Inyeccion de dependencias</a></li>
                    <li>
                        <a href="<?= Url::to(['site/test-db1']) ?>">
                            #48 - Objetos de acceso de datos</a> /
                        <a href="<?= Url::to(['site/test-db2']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/test-db3']) ?>">
                            Ejercicio 3 </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['site/query-builder1']) ?>">
                            #49 - Constructor de consultas</a> /
                        <a href="<?= Url::to(['site/query-builder2']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/query-builder3']) ?>">
                            Ejercicio 3 </a> /
                        <a href="<?= Url::to(['site/query-builder4']) ?>">
                            Ejercicio 4 </a> /
                        <a href="<?= Url::to(['site/query-builder5']) ?>">
                            Ejercicio 5 </a> /
                        <a href="<?= Url::to(['site/query-builder6']) ?>">
                            Ejercicio 6 </a> /
                        <a href="<?= Url::to(['site/query-builder7']) ?>">
                            Ejercicio 7 </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['site/active-record1']) ?>">
                            #50 - Active Record</a> /
                        <a href="<?= Url::to(['site/active-record2']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/active-record3']) ?>">
                            Ejercicio 3 </a> /
                        <a href="<?= Url::to(['site/active-record4']) ?>">
                            Ejercicio 4 </a> /
                        <a href="<?= Url::to(['site/active-record5']) ?>">
                            Ejercicio 5 </a>
                    </li>
                    <li><a href="<?= Url::to(['site/about2']) ?>">
                            #52 - Tematización</a></li>
                    <li><a href="<?= Url::to('@web/rest-user', true) ?>">
                            #54 - API Restful en accion (Postman para manipular datos)</a></li>
                    <li>
                        <a href="<?= Url::to('@web/rest-user?expand=extraField-email', true) ?>">
                            #55 - Campos extra para APIs y modificacion de acciones</a> /
                        <a href="<?= Url::to('@web/rest-user?expand=extraField-email,extraField-time', true) ?>">
                            Ejercicio 2 </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['site/using-cache1']) ?>">
                            #57 - Cache</a> /
                        <a href="<?= Url::to(['site/using-cache2']) ?>">
                            Ejercicio 2 </a>
                    </li>
                    <li><a href="<?= Url::to(['site/fragment-caching']) ?>">
                            #58 - Cache de fragmentos</a></li>
                    <li><a href="<?= Url::to(['site/usando-alias']) ?>">
                            #59 - Usando alias</a></li>
                    <li><a href="<?= Url::to(['site/usando-logs']) ?>">
                            #60 - Usando logs</a></li>
                    <li>
                        <a href="<?= Url::to(['site/show-error1']) ?>">
                            #61 - Manejo de errores</a> /
                        <a href="<?= Url::to(['site/show-error2']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/show-error3']) ?>">
                            Ejercicio 3 </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['site/auth1']) ?>">
                            #62 - Autenticacion</a> /
                        <a href="<?= Url::to(['site/auth2']) ?>">
                            Ejercicio 2 </a>
                    </li>
                    <li><a href="<?= Url::to(['site/auth']) ?>">
                            #63 - Autorizacion</a></li>
                    <li>
                        <a href="<?= Url::to(['site/translation1']) ?>">
                            #64 - Traduccion</a> /
                        <a href="<?= Url::to(['site/translation2']) ?>">
                            Ejercicio 2 </a> /
                        <a href="<?= Url::to(['site/translation3']) ?>">
                            Ejercicio 3</a>
                    </li>
                    <li><a href="<?= Url::to(['/user']) ?>">
                            #66 - Creando un modelo (con Gii)</a></li>
                    <li>
                        <a href="<?= Url::to(['/custom']) ?>">
                            #67 - Generando un controlador (con Gii)</a> /
                        <a href="<?= Url::to(['/custom/hello']) ?>">
                            Vista 2 </a> /
                        <a href="<?= Url::to(['/custom/world']) ?>">
                            Vista 3 </a> /
                        <a href="<?= Url::to(['/custom/customview']) ?>">
                            Ejercicio 2 </a>
                    </li>
                    <li><a href="<?= Url::to(['/admin/default']) ?>">
                            #68 - Generando un modulo (con Gii)</a></li>
                </ul>
            </div>
        </div>
    </div>