<?php
   use yii\grid\GridView;

$this->title = 'Class 39 - GridView Widget';
   
      echo GridView::widget([
      'dataProvider' => $dataProvider,
      'columns' => [
         'id',
         [
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
            'label' => 'Nombre / correo',
            'value' => function ($data) {
                return $data->name . " / " . $data->email;
            },
         ],
      ],
   ]);
