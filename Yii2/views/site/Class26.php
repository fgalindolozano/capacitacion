<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Class 26 - AdHoc Validations';
?>

<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'validation-form']); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'email')->input('email') ?>
        <?= $form->field($model, 'country') ?>
        <?= $form->field($model, 'city') ?>
        <?= $form->field($model, 'phone') ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary',
'name' => 'validation-button']) ?>
        </div>
        <div class="form-group">
            <span>Estas validaciones solo funcionan server-side. <br> Osea, no se van a ver aqui u_u</span>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>