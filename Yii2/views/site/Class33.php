<?php
   use yii\widgets\LinkPager;

   $this->title = 'Class 33 - Pagination';
?>
<?php foreach ($models as $model): ?>
   <?= $model->id; ?>
   <?= $model->name; ?>
   <?= $model->email; ?>
   <br/>
<?php endforeach; ?>
<?php
   // display pagination
   echo LinkPager::widget([
      'pagination' => $pagination,
   ]);
?>
<div class="form-group">
    <span> Crear una base de datos con el siguiente comando: </span> <br>
    <code> CREATE DATABASE Yii CHARACTER SET utf8 COLLATE utf8_general_ci; </code>
</div>
