<?php 
   use app\components\FirstWidget; 
   $this->title = 'Class 11 - Widgets';
?> 
<?= FirstWidget::widget() ?>

<?php FirstWidget::begin(); ?>
   <h3 style="color:red">This is a test content in-between Widget::begin and Widget::end </h3>
<?php FirstWidget::end(); ?>