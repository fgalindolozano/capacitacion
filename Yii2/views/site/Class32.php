<?php
   $formatter = \Yii::$app->formatter;
   
   $this->title = 'Class 32 - Formatting';

   echo "<h3>Parte 1 (Formateos basicos)</h3>";
   echo $formatter->asDate('2016-01-01', 'long'),"<br>";
   echo $formatter->asPercent(0.515, 2),"<br>";
   echo $formatter->asEmail('test@test.com'),"<br>";
   echo $formatter->asBoolean(true),"<br>";
   echo $formatter->asDate(null),"<br>";

   echo "<h3>Parte 2 (Formateo de tiempo)</h3>";

   echo $formatter->asDate(date('Y-m-d'), 'long'),"<br>";
   echo $formatter->asTime(date("Y-m-d")),"<br>";
   echo $formatter->asDatetime(date("Y-m-d")),"<br>";
   echo $formatter->asTimestamp(date("Y-m-d")),"<br>";
   echo $formatter->asRelativeTime(date("Y-m-d")),"<br>";

   echo "<h3>Parte 3 (Formateo de fechas)</h3>";
   echo $formatter->asDate(date('Y-m-d'), 'short'),"<br>";
   echo $formatter->asDate(date('Y-m-d'), 'medium'),"<br>";
   echo $formatter->asDate(date('Y-m-d'), 'long'),"<br>";
   echo $formatter->asDate(date('Y-m-d'), 'full'),"<br>";

   echo "<h3>Parte 4 (Formateo de numeros)</h3>";
   echo $formatter->asInteger(105),"<br>";
   echo $formatter->asDecimal(105.41),"<br>";
   echo $formatter->asPercent(0.51),"<br>";
   echo $formatter->asScientific(105),"<br>";
   echo $formatter->asCurrency(105, "$"),"<br>";
   echo $formatter->asSize(105),"<br>";
   echo $formatter->asShortSize(105),"<br>";

   echo "<h3>Parte 5 (Formateo basado en localidad)</h3>";
   Yii::$app->formatter->locale = 'ru-RU';
   echo Yii::$app->formatter->asDate('2016-01-01'),"<br>";
   Yii::$app->formatter->locale = 'de-DE';
   echo Yii::$app->formatter->asDate('2016-01-01'),"<br>";
   Yii::$app->formatter->locale = 'en-US';
   echo Yii::$app->formatter->asDate('2016-01-01'),"<br>";
   echo "<br><span><b>Nota:</b> No funciona :c </span>";