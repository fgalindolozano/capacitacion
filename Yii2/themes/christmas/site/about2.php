<?php
   /* @var $this yii\web\View */
   use yii\helpers\Html;
   use yii\helpers\Url;
   $this->title = 'About';
   $this->params['breadcrumbs'][] = $this->title;
   $this->registerMetaTag(['name' => 'keywords', 'content' => 'yii, developing,
      views, meta, tags']);
   $this->registerMetaTag(['name' => 'description', 'content' => 'This is the
      description of this page!'], 'description');
?>

<div class="site-about">
    <h2>Christmas theme</h2>
    <img src="<?= Url::to('@web/images/xmas-tree.png'); ?>">
    <?= Html::img('@web/images/xmas-tree.png'); ?>
    <p style="color: red;">
        Este es un about personalizado
    </p>
</div>