<?php
namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class MyUserFixture extends ActiveFixture
{
    public $modelClass = 'app\models\MyUser';
}