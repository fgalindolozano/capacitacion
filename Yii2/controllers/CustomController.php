<?php

namespace app\controllers;

use Yii;

class CustomController extends \yii\web\Controller
{
    public function actionHello()
    {
        return $this->render('hello');
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionWorld()
    {
        return $this->render('world');
    }

    public function actionCustomview()
    {
        $model = new \app\models\MyUser();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('/customview', [
        'model' => $model,
    ]);
    }
}
