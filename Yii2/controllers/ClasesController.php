<?php
   
   namespace app\controllers;

use yii\web\Controller;

   class ClasesController extends Controller
   {
       #Layout especial creado en el tutorial 14
       public $layout = 'newlayout';

       public $defaultAction = "index";
    
       public function actions()
       {
           return [
           'test' => 'app\components\TestAction',
        ];
       }
       public function actionIndex()
       {
           return $this->render('../site/index');
       }
       public function actionClass4($message = "Default msg")
       {
           return $this->render("clase4", ['message' => $message]);
       }
       public function actionClass5()
       {
           return $this->render('clase5');
       }
       public function actionClass6()
       {
           return $this->render('clase6');
       }
       public function actionClass8()
       {
           $message = "Default";
           return $this->render("clase8", ['message' => $message ]);
       }
       public function actionClass9()
       {
           return $this->render('clase9');
       }
       public function actionSimple()
       {
           return "<h1>This is a simplistic view</h1>";
       }
       public function actionGoToFb()
       {
           return $this->redirect('http://facebook.com');
       }
       public function actionParameters($fs, $sc)
       {
           return "$fs $sc";
       }
       public function actionClass17()
       {
           return $this->render('clase17');
       }
   }
