<?php
namespace app\controllers;

use yii\rest\ActiveController;

class RestUserController extends ActiveController
{
    public $modelClass = 'app\models\MyUser';
    //Clase 53
    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['delete'], $actions['create']);
        return $actions;
    }
}
