<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegistrationForm; #Es imperativo importar el modelo aqui
use app\models\ValidationForm;
use yii\base\DynamicModel;
use app\models\RegFormClass26;
use app\models\RegFormClass262;
use app\models\UploadFileClass31;
use yii\web\UploadedFile;
use app\models\MyUser;
use yii\data\Pagination;
use yii\data\Sort;
use app\components\Taxi;
use yii\data\ActiveDataProvider;
use yii\data\SQLDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use app\models\User;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            [
                'class' => 'yii\filters\PageCache',
                'only' => ['index'],
                'duration' => 10
             ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        $model->scenario = ContactForm::SCENARIO_EMAIL_FROM_USER;
        if ($model->load(Yii::$app->request->post()) && $model->
           contact(Yii::$app->params ['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
           'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $email = "admin@support.com";
        $phone = "+78007898100";
        return $this->render('about', [
           'email' => $email,
           'phone' => $phone
        ]);
    }

    public function actionShowCm()
    {
        $mContactForm = new \app\models\ContactForm();
        $mContactForm->name = "Formulario de contacto";
        $mContactForm->email = "user@test.com";
        $mContactForm->subject = "Test";
        $mContactForm->body = "Esta es una prueba";
        highlight_string("<?php\n\n" . var_export($mContactForm, true)
                                    . var_export($mContactForm->attributes, true) . ";\n\nJSON:\n\n");
        #Deberia soltar organizadito eso, pero por el string anterior no ocurre.
        echo \yii\helpers\Json::encode($mContactForm, JSON_PRETTY_PRINT);
    }

    public function actionTestWidget()
    {
        return $this->render('testwidget');
    }

    public function actionRequest0()
    {
        highlight_string("<?php\n\n" . var_export(Yii::$app->request->get(), true).";\n");
    }

    public function actionRequest1()
    {
        $req = Yii::$app->request;
        if ($req->isAjax) {
            echo "Esta es una peticion AJAX";
        }
        if ($req->isGet) {
            echo "Esta es una peticion GET";
        }
        if ($req->isPost) {
            echo "Esta es una peticion POST";
        }
        if ($req->isPut) {
            echo "Esta es una peticion PUT";
        }
    }

    public function actionRequest2()
    {
        //the URL without the host
        var_dump(Yii::$app->request->url);

        //the whole URL including the host path
        var_dump(Yii::$app->request->absoluteUrl);
        
        //the host of the URL
        var_dump(Yii::$app->request->hostInfo);
        
        //the part after the entry script and before the question mark
        var_dump(Yii::$app->request->pathInfo);
        
        //the part after the question mark
        var_dump(Yii::$app->request->queryString);
        
        //the part after the host and before the entry script
        var_dump(Yii::$app->request->baseUrl);
        
        //the URL without path info and query string
        var_dump(Yii::$app->request->scriptUrl);
        
        //the host name in the URL
        var_dump(Yii::$app->request->serverName);
        
        //the port used by the web server
        var_dump(Yii::$app->request->serverPort);
    }

    public function actionRequest3()
    {
        var_dump(Yii::$app->request->headers);
    }

    public function actionRequest4()
    {
        var_dump(Yii::$app->request->userHost);
        var_dump(Yii::$app->request->userIP);
    }

    public function actionResponse0()
    {
        Yii::$app->response->statusCode = 201;
        echo "Codigo de Estatus: ".Yii::$app->response->statusCode;
    }

    public function actionResponse1()
    {
        throw new \yii\web\HttpException(429);
    }

    public function actionResponse2()
    {
        Yii::$app->response->headers->add('Pragma', 'no-cache');
        var_dump(Yii::$app->response->headers);
    }

    public function actionResponse3()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
           'id' => '1',
           'name' => 'Ivan',
           'age' => 24,
           'country' => 'Poland',
           'city' => 'Warsaw'
        ];
    }

    public function actionResponse4()
    {
        return $this->redirect('http://www.tutorialspoint.com/');
    }

    public function actionResponse5()
    {
        return \Yii::$app->response->sendFile('favicon.ico');
    }

    public function actionRoutes0()
    {
        return $this->render('routes0');
    }
    public function actionRoutes1()
    {
        return $this->render('routes1');
    }

    public function actionRegistration()
    {
        $mRegistration = new RegistrationForm();
        return $this->render('registration', ['model' => $mRegistration]);
    }
    public function actionValidation()
    {
        $model = new ValidationForm();
        return $this->render('validation', ['model' => $model]);
    }
    
    public function actionSpecialValidation1()
    {
        $model = DynamicModel::validateData([
            'username' => 'John',
            'email' => 'john@gmail.com'
         ], [
            [['username', 'email'], 'string', 'max' => 12],
            ['email', 'email'],
         ]);
          
        if ($model->hasErrors()) {
            var_dump($model->errors);
        } else {
            echo "success";
        }
    }
    public function actionSpecialValidation2()
    {
        $model = new RegFormClass26();
        return $this->render('class26', ['model' => $model]);
    }
    public function actionSpecialValidation3()
    {
        $model = new RegFormClass262();
        return $this->render('class26', ['model' => $model]);
    }
    
    public function actionAjaxValidation()
    {
        $model = new ValidationForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request>post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        return $this->render('class27', ['model' => $model]);
    }
    
    public function actionSesiones()
    {
        $session = Yii::$app->session;
        // open a session
        $session->open();
        // check if a session is already opened
        if ($session->isActive) {
            echo "session is active";
        }
        // close a session
        $session->close();
        // destroys all data registered to a session
        $session->destroy();
    }
    public function actionSesionesDatos()
    {
        $session = Yii::$app->session;
         
        // set a session variable
        $session->set('language', 'es-CO');
         
        // get a session variable
        $language = $session->get('language');
        var_dump($language);
               
        // remove a session variable
        $session->remove('language');
               
        // check if a session variable exists
        if (!$session->has('language')) {
            echo "language is not set";
        }
               
        $session['captcha'] = [
           'value' => 'aSBS23',
           'lifetime' => 7200,
        ];
        var_dump($session['captcha']);
    }
    
    public function actionFlashData()
    {
        $session = Yii::$app->session;
        // set a flash message named as "greeting"
        $session->setFlash('greeting', 'Hola visitante!');
        return $this->render('class29');
    }
    
    public function actionReadCookies()
    {
        // get cookies from the "request" component
        $cookies = Yii::$app->request->cookies;
        // get the "language" cookie value
        // if the cookie does not exist, return "ru" as the default value
        $language = $cookies->getValue('language', 'ru');
        // an alternative way of getting the "language" cookie value
        if (($cookie = $cookies->get('language')) !== null) {
            $language = $cookie->value;
        }
        // you may also use $cookies like an array
        if (isset($cookies['language'])) {
            $language = $cookies['language']->value;
        }
        // check if there is a "language" cookie
        if ($cookies->has('language')) {
            echo "Current language: $language";
        }
    }
    public function actionSendCookies()
    {
        // get cookies from the "response" component
        $cookies = Yii::$app->response->cookies;
        // add a new cookie to the response to be sent
        $cookies->add(new \yii\web\Cookie([
           'name' => 'language',
           'value' => 'es-CO',
        ]));
        $cookies->add(new \yii\web\Cookie([
           'name' => 'username',
           'value' => 'John',
        ]));
        $cookies->add(new \yii\web\Cookie([
           'name' => 'country',
           'value' => 'USA',
        ]));
    }
    
    public function actionSubirArchivos()
    {
        $model = new UploadFileClass31();
        if (Yii::$app->request->isPost) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->upload()) {
                // file is uploaded successfully
                echo "File successfully uploaded";
                return;
            }
        }
        return $this->render('class31', ['model' => $model]);
    }
    
    public function actionFormateador()
    {
        return $this->render('class32');
    }
    
    public function actionPaginacion()
    {
        $query = MyUser::find();
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'defaultPageSize' => 6]);
        $models = $query->offset($pagination->offset)
           ->limit($pagination->limit)
           ->all();
        return $this->render('class33', [
           'models' => $models,
           'pagination' => $pagination,
        ]);
    }
    
    public function actionSorting()
    {
        $sort = new Sort([
           'attributes' => ['id', 'name', 'email'],
        ]);
        $models = MyUser::find()
           ->orderBy($sort->orders)
           ->all();
        return $this->render('class34', [
           'models' => $models,
           'sort' => $sort,
        ]);
    }
    
    public function actionObjetos()
    {
        $object = new Taxi();
        $phone = $object->phone;
        var_dump($phone);
        $object->phone = '79005448877';
        var_dump($object);
    }
    
    public function actionProveedoresDeDatos1()
    {
        $query = MyUser::find();
        $provider = new ActiveDataProvider([
           'query' => $query,
           'pagination' => [
              'pageSize' => 2,
           ],
        ]);
        $users = $provider->getModels();
        echo "<pre>";
        var_dump($users);
    }
    public function actionProveedoresDeDatos2()
    {
        $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM user')->queryScalar();
        $provider = new SqlDataProvider([
           'sql' => 'SELECT * FROM user',
           'totalCount' => $count,
           'pagination' => [
              'pageSize' => 5,
           ],
           'sort' => [
              'attributes' => [
                 'id',
                 'name',
                 'email',
              ],
           ],
        ]);
        $users = $provider->getModels();
        echo "<pre>";
        var_dump($users);
    }
    public function actionProveedoresDeDatos3()
    {
        $data = MyUser::find()->asArray()->all();
        $provider = new ArrayDataProvider([
           'allModels' => $data,
           'pagination' => [
              'pageSize' => 3,
           ],
           'sort' => [
              'attributes' => ['id', 'name'],
           ],
        ]);
        $users = $provider->getModels();
        echo "<pre>";
        var_dump($users);
    }
    
    public function actionDataWidget()
    {
        $model = MyUser::find()->one();
        return $this->render('class37', [
           'model' => $model
        ]);
    }
    
    public function actionDataListWidget()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => MyUser::find(),
           'pagination' => [
              'pageSize' => 20,
           ],
        ]);
        return $this->render('class38', [
           'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionDataGridWidget1()
    {
        $dataProvider = new ActiveDataProvider([
               'query' => MyUser::find(),
               'pagination' => [
                  'pageSize' => 20,
               ],
            ]);
        return $this->render('class39-1', [
               'dataProvider' => $dataProvider
            ]);
    }
    public function actionDataGridWidget2()
    {
        $dataProvider = new ActiveDataProvider([
               'query' => MyUser::find(),
               'pagination' => [
                  'pageSize' => 20,
               ],
            ]);
        return $this->render('class39-2', [
               'dataProvider' => $dataProvider
            ]);
    }
    public function actionDataGridWidget3()
    {
        $dataProvider = new ActiveDataProvider([
               'query' => MyUser::find(),
               'pagination' => [
                  'pageSize' => 20,
               ],
            ]);
        return $this->render('class39-3', [
               'dataProvider' => $dataProvider
            ]);
    }
    
    public function actionTestEvent()
    {
        $model = new MyUser();
        $model->name = "John";
        $model->email = "john@gmail.com";
        if ($model->save()) {
            $model->trigger(MyUser::EVENT_NEW_USER);
        }
    }
    
    public function actionTestBehavior()
    {
        //creating a new user
        $model = new MyUser();
        $model->name = "Admin";
        $model->email = "admin@gmail.com";
        if ($model->save()) {
            var_dump(MyUser::find()->asArray()->all());
        }
    }
    
    public function actionTestInterface()
    {
        $container = new \yii\di\Container();
        $container->set("\app\components\MyInterface", "\app\components\First");
        $obj = $container->get("\app\components\MyInterface");
        $obj->test(); // print "First class"
        $container->set("\app\components\MyInterface", "\app\components\Second");
        $obj = $container->get("\app\components\MyInterface");
        $obj->test(); // print "Second class"
    }
    
    public function actionTestDb1()
    {
        //Formato
        echo "<pre>";
        // return a set of rows. each row is an associative array of column names and values.
        // an empty array is returned if the query returned no results
        $users = Yii::$app->db->createCommand('SELECT * FROM user LIMIT 5')
           ->queryAll();
        var_dump($users);
        // return a single row (the first row)
        // false is returned if the query has no result
        $user = Yii::$app->db->createCommand('SELECT * FROM user WHERE id=1')
           ->queryOne();
        var_dump($user);
        // return a single column (the first column)
        // an empty array is returned if the query returned no results
        $userName = Yii::$app->db->createCommand('SELECT name FROM user')
           ->queryColumn();
        var_dump($userName);
        // return a scalar value
        // false is returned if the query has no result
        $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM user')
           ->queryScalar();
        var_dump($count);
    }
    public function actionTestDb2()
    {
        //Formato
        echo "<pre>";
        $firstUser = Yii::$app->db->createCommand('SELECT * FROM user WHERE id = :id')
           ->bindValue(':id', 1)
           ->queryOne();
        var_dump($firstUser);
        $params = [':id' => 2, ':name' => 'User2'];
        $secondUser = Yii::$app->db->createCommand('SELECT * FROM user WHERE
           id = :id AND name = :name')
           ->bindValues($params)
           ->queryOne();
        var_dump($secondUser);
        //another approach
        $params = [':id' => 3, ':name' => 'User3'];
        $thirdUser = Yii::$app->db->createCommand('SELECT * FROM user WHERE
           id = :id AND name = :name', $params)
           ->queryOne();
        var_dump($thirdUser);
    }
    public function actionTestDb3()
    {
        //Formato
        echo "<pre>";
        // INSERT (table name, column values)
        Yii::$app->db->createCommand()->insert('user', [
           'name' => 'My New User',
           'email' => 'mynewuser@gmail.com',
        ])->execute();
        $user = Yii::$app->db->createCommand('SELECT * FROM user WHERE name = :name')
           ->bindValue(':name', 'My New User')
           ->queryOne();
        var_dump($user);
        // UPDATE (table name, column values, condition)
        Yii::$app->db->createCommand()->update('user', ['name' => 'My New User
           Updated'], 'name = "My New User"')->execute();
        $user = Yii::$app->db->createCommand('SELECT * FROM user WHERE name = :name')
           ->bindValue(':name', 'My New User Updated')
           ->queryOne();
        var_dump($user);
        // DELETE (table name, condition)
        Yii::$app->db->createCommand()->delete('user', 'name = "My New User
           Updated"')->execute();
        $user = Yii::$app->db->createCommand('SELECT * FROM user WHERE name = :name')
           ->bindValue(':name', 'My New User Updated')
           ->queryOne();
        var_dump($user);
    }
    
    public function actionQueryBuilder1()
    {
        //Formato
        echo "<pre>";
        $user = (new Query())->select(['id', 'name', 'email'])->from('user')->
        where(['name' => 'User10'])->one();
        var_dump($user);
    }
    public function actionQueryBuilder2()
    {
        //Formato
        echo "<pre>";
        $user = (new Query())->select(['id', 'name', 'email'])->from('user')->
        where('name = :name', [':name' => 'User11'])->one();
        var_dump($user);
    }
    public function actionQueryBuilder3()
    {
        //Formato
        echo "<pre>";
        $user = (new Query())->select(['id', 'name', 'email'])->from('user')
        ->where(['name' => 'User5','email' => 'user5@gmail.com'])->one();
        var_dump($user);
    }
    public function actionQueryBuilder4()
    {
        //Formato
        echo "<pre>";
        $user = (new Query())->select(['id', 'name', 'email'])->from('user')
        ->where(['between', 'id', 5, 7])->all();
        var_dump($user);
    }
    public function actionQueryBuilder5()
    {
        //Formato
        echo "<pre>";
        $user = (new Query())->select(['id', 'name', 'email'])->from('user')
        ->orderBy('name DESC')->all();
        var_dump($user);
    }
    public function actionQueryBuilder6()
    {
        //Formato
        echo "<pre>";
        $user = (new Query())->select(['id', 'name', 'email'])->from('user')
        ->groupBy('name')->having('id < 5')->all();
        var_dump($user);
    }
    public function actionQueryBuilder7()
    {
        //Formato
        echo "<pre>";
        $user = (new Query())->select(['id', 'name', 'email'])->from('user')
        ->limit(5)->offset(5)->all();
        var_dump($user);
    }
    
    public function actionActiveRecord1()
    {
        $data[0] = MyUser::find()->where(['id' => 1])->one();
        $data[1] = MyUser::find()->count();
        $data[2] = MyUser::find()->orderBy('id')->all();
        return Json::encode($data);
    }
    public function actionActiveRecord2()
    {
        $data[0] = MyUser::findOne(1);
        $data[1] = MyUser::findAll([1, 2, 3, 4]);
        $data[2] = MyUser::findOne(['id' => 5]);
        return Json::encode($data);
    }
    public function actionActiveRecord3()
    {
        //Formato
        echo "<pre>";
        $user = new MyUser();
        $user->name = 'MyCustomUser2';
        $user->email = 'mycustomuser@gmail.com';
        $user->save();
        var_dump($user->attributes);
        $user = MyUser::findOne(['name' => 'MyCustomUser2']);
        $user->email = 'newemail@gmail.com';
        $user->save();
        var_dump($user->attributes);
    }
    public function actionActiveRecord4()
    {
        $user = MyUser::findOne(['name' => 'MyCustomUser2']);
        if (isset($user)) {
            if ($user->delete()) {
                echo "Deleted";
            }
        } else {
            echo "No existe el usuario a borrar";
        }
    }
    public function actionActiveRecord5()
    {
        MyUser::deleteAll('id >= 20');
        echo "No funciona, ya que no hay tantos usuarios en la db";
    }
    
    public function actionAbout2()
    {
        return $this->render('about2');
    }
    
    public function actionUsingCache1()
    {
        //Crea la variable del cache
        $cache = Yii::$app->cache;
        // try retrieving $data from cache
        $data = $cache->get("my_cached_data");
        if ($data === false) {
            // $data is not found in cache, calculate it from scratch
            $data = date("d.m.Y H:i:s");
            // store $data in cache so that it can be retrieved next time
            $cache->set("my_cached_data", $data, 10);
        }
        // $data is available here
        var_dump($data);
    }
    public function actionUsingCache2()
    {
        //Formato
        echo "<pre>";
        $duration = 10;
        $result = MyUser::getDb()->cache(function ($db) {
            return MyUser::find()->count();
        }, $duration);
        var_dump($result);
        Yii::$app->db->createCommand()->delete('user', 'name = "cached user name"')->execute();
        $user = new MyUser();
        $user->name = "cached user name";
        $user->email = "cacheduseremail@gmail.com";
        $user->save();
        echo "========== <br>";
        var_dump(MyUser::find()->count());
    }
    
    public function actionFragmentCaching()
    {
        $user = new MyUser();
        $user->name = "cached user name";
        $user->email = "cacheduseremail@gmail.com";
        $user->save();
        $models = MyUser::find()->all();
        return $this->render('class56', ['models' => $models]);
    }
    //Class 57
    public function actionUsandoAlias()
    {
        //Formato
        echo "<pre>";
        Yii::setAlias("@components", "@app/components");
        Yii::setAlias("@imagesUrl", "@web/images");
        var_dump(Yii::getAlias("@components"));
        var_dump(Yii::getAlias("@imagesUrl"));
    }
    
    public function actionUsandoLogs()
    {
        Yii::trace('trace log message');
        Yii::info('info log message');
        Yii::warning('warning log message');
        Yii::error('error log message');
        echo "Logs creados exitosamente. <br><br>";
        echo Html::a(
            'Ver el final de este archivo para revisarlos (Toma un poco en cargar)',
            Url::to("@web/../runtime/logs/app.log", true)
        );
    }
    
    public function actionShowError1()
    {
        try {
            5/0;
        } catch (ErrorException $e) {
            Yii::warning("Ooops...division by zero.");
        }
    }
    public function actionShowError2()
    {
        throw new NotFoundHttpException("Este es un error personalizado");
    }
    
    public function actionAuth1()
    {
        //Formato
        echo "<pre>";
        // the current user identity. Null if the user is not authenticated.
        $identity = Yii::$app->user->identity;
        var_dump($identity);
        // the ID of the current user. Null if the user not authenticated.
        $id = Yii::$app->user->id;
        var_dump($id);
        // whether the current user is a guest (not authenticated)
        $isGuest = Yii::$app->user->isGuest;
        var_dump($isGuest);
    }
    public function actionAuth2()
    {
        //Formato
        echo "<pre>";
        // whether the current user is a guest (not authenticated)
        var_dump(Yii::$app->user->isGuest);
        // find a user identity with the specified username.
        // note that you may want to check the password if needed
        $identity = User::findByUsername("admin");
        // logs in the user
        Yii::$app->user->login($identity);
        // whether the current user is a guest (not authenticated)
        var_dump(Yii::$app->user->isGuest);
        Yii::$app->user->logout();
        // whether the current user is a guest (not authenticated)
        var_dump(Yii::$app->user->isGuest);
    }
    public function actionAuth()
    {
        //Formato
        echo "<pre>";
        //Example
        $password = "asd%#G3";
        //generates password hasg
        $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        var_dump($hash);
        //validates password hash
        if (Yii::$app->getSecurity()->validatePassword($password, $hash)) {
            echo "correct password<br>";
        } else {
            echo "incorrect password<br>";
        }
        //generate a token
        $key = Yii::$app->getSecurity()->generateRandomString();
        var_dump($key);
        //encrypt data with a secret key
        $encryptedData = Yii::$app->getSecurity()->encryptByPassword("mydata", $key);
        var_dump($encryptedData);
        //decrypt data with a secret key
        $data = Yii::$app->getSecurity()->decryptByPassword($encryptedData, $key);
        var_dump($data);
        //hash data with a secret key
        $data = Yii::$app->getSecurity()->hashData("mygenuinedata", $key);
        var_dump($data);
        //validate data with a secret key
        $data = Yii::$app->getSecurity()->validateData($data, $key);
        var_dump($data);
    }
    public function actionTranslation1()
    {
        //Formato
        echo "<pre>";
        \Yii::$app->language = 'ru-RU';
        $msg='This is a string to translate!';
        echo 'Mensaje Ingles (USA): '.$msg.'<br>';
        echo 'Mensaje Ruso (Rusia): '.\Yii::t('app', $msg);
    }
    public function actionTranslation2()
    {
        //Formato
        echo "<pre>";
        \Yii::$app->language = 'ru-RU';
        $username = \Yii::t('app', 'Vladimir');
        // display a translated message with username being "Vladimir"
        echo \Yii::t('app', 'Hello, {username}!', ['username' => $username,]), "<br>";
        $username = \Yii::t('app', 'John');
        // display a translated message with username being "John"
        echo \Yii::t('app', 'Hello, {username}!', ['username' => $username,]), "<br>";
        $price = 150;
        $count = 3;
        $subtotal = $price*$count;
        echo \Yii::t('app', 'Price: {0}, Count: {1}, Subtotal: {2}', [$price, $count, $subtotal]);
    }
    public function actionTranslation3()
    {
        \Yii::$app->language = 'ru-RU';
        return $this->render('index');
    }
}
