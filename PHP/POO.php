<?php

//VIDEO 26 - Creando instancias de objetos en PHP

/*
Como llamar un fichero externo de PHP:
include() => "pega" el contenido del archivo en el archivo actual; si hay
contratiempos, solo genera una excepcion, y continua la ejecucion.
require() => "pega" el contenido del archivo en el archivo actual; si hay
contratiempos, genera un error fatal, y detiene la ejecucion.
include_once() => igual que include, pero si el contenido fue anteriormente
llamado, no realiza una llamada adicional.
require_once() => igual que require, pero si el contenido fue anteriormente
llamado, no realiza una llamada adicional.
*/

include_once('POO/Persona.php'); //Llamado al archivo que tiene la clase padre
$obj1=new Persona(); //Creacion de una instancia -una copia- de la clase
$obj1->showEdad(1950); //Uso de una de los metodos publicos
$obj1->setNombre("Test"); //Uso de set (individual)
echo "(1) Bienvenido, ".$obj1->getNombre().". <br />"; //Uso de get (idem)
$obj1->nombre="Test"; //Uso de set (metodo magico)
echo "(2) Bienvenido, ".$obj1->nombre.". <br />"; //Uso de get (idem)
echo "(1) Año actual registrado : ".$obj1::$annoActual."<br />";
//Manipulacion propiedades estaticas. Manera #1 ↑, Manera #2 ↓
Persona::$annoActual=date('M d, Y'); //<- Cambio para el VIDEO 27
echo "(2) Nuevo año registrado : ".Persona::$annoActual."<br />";

//VIDEO 26 - Herencia de objetos en PHP

Persona::$annoActual=date('Y'); //<- Cambio para el VIDEO 27
echo "<br />";
require_once('POO/Vendedor.php'); //Llamado al archivo que tiene la clase hija
$obj2=new Vendedor(); //Lease linea 17.
$obj2->showEdad(2000); //Uso de uno de los metodos publicos del padre
$obj2->futureAge(2000); //Uso de uno de los metodos publicos del hijo

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
 ?>
