<?php
//VIDEO 29 - Creacion y manipulacion de sesiones de PHP 7

//Datos de verificacion
$user='admin';
$pass='pass';
$msg='';

//Mensaje inicial de la pagina, si las cookies no existen
if(!isset($_COOKIE['user']) && !isset($_COOKIE['pass'])) {
  $msg='No existen cookies';
} else {
  $msg="Sesion iniciada. Bienvenido ".$_COOKIE['user']." (".$_COOKIE['pass'].")";
}
/*
Si el usuario envio datos, verificar si dichos datos concuerdan con los datos
que posee PHP, si esto es asi, iniciar la sesion, y actualizar el mensaje.
*/
if(isset($_POST['user']) && isset($_POST['pass'])) {
  if($_POST['user']==$user && $_POST['pass']==$pass){
    setcookie('user',$user,time()+60*60*24*30);
    setcookie('pass',$pass,time()+60*60*24*30);
  } else {
    $msg="Nombre de usuario o contraseña incorrectos, intente de nuevo";
  }
}

//Como modificar cookies
//setcookie(nombre,content);

//Como borrar cookies
//unset($_COOKIE[nombre]);
//setcookie(nombre,null,-1);

 ?>

<style>

html { font-size: 18px; }
body { margin: 2.5vw 5vw; text-align: justify; font-family: Tahoma, Geneva, sans-serif }
table { line-height: 32px; }
thead p { display: block; text-align: center; }
td:nth-child(1) { padding-right: 2.5rem; }
td:nth-child(2) { max-width: 20vw; }
tbody td:nth-child(2) p, tbody tr:nth-child(3) p { display: block; text-align: center; }
td input { width: 80%; }
tbody tr:nth-child(3) td:nth-child(1) { padding-right: 0; }
input[type='submit']{ margin-top: 1vw; }

</style>

<form method='post' action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <table>
    <thead>
      <tr>
        <td colspan='2'>
          <p><b>Formulario de login</b></p>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><p>Usuario</p></td>
        <td><p><input type='text' name='user' /></p></td>
      </tr>
      <tr>
        <td><p>Contraseña</p></td>
        <td><p><input type='password' name='pass' /></p></td>
      </tr>
      <tr>
        <td colspan='2'>
          <p><input type='submit' name='submit' /></p>
        </td>
      </tr>
    </tbody>
  </table>
</form>

<p> <?php echo $msg; ?> </p>
