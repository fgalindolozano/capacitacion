<?php
//VIDEO 25 - Creando objetos en PHP

class Persona{
  //Declaracion de propiedades
  private $nombre;
  private $apellido;
  public static $annoActual=2018;
  //Metodo magico #1 : Constructor
  public function __constructor(){
    $this->nombre="";
    $this->apellido="";
  }

  //Metodo magico #2 : Set (general)
  public function __set($property,$value){
    $this->$property=$value;
  }
  //Metodo magico #3 : Get (general)
  public function __get($property){
    return $this->$property;
  }

  /*
  Ventajas/razones de crear individualmente sets/gets:
  -Si alguna propiedad requiere algun tipo de logica especial (e.g. una operacion
  matematica antes de devolver o asignarle su valor).
  -Permite establecer patrones de tipado, haciendo mas claras las intenciones
  del desarrollador original, asi, favorece la legibilidad/mantenimiento del codigo
  */

  //Set individual #1 (Ventajas: tipado de argumentos, logica especial)
  public function setNombre(string $nombre)
  {
      $this->nombre = $nombre;
  }
  //Get individual #1 (Ventajas: transformacion/tipado, logica especial)
  public function getNombre():string  {
      return $this->nombre;
  }
  //Set individual #2
  public function setApellido(string $apellido)
  {
      $this->apellido = $apellido;
  }
  //Get individual #2
  public function getApellido():string  {
      return $this->apellido;
  }

  //Metodo privado : Calcular edad (manipulacion de propiedades estaticas)
  protected function calcEdad($arg){
    return self::$annoActual-$arg;
  }
  //Metodo publico : Mostrar edad
  public function showEdad($arg){
    echo "Tienes ".$this->calcEdad($arg). " años.<br />";
  }
  //Metodo magico #4 : Get (general)
  public function __destruct(){
    echo "<br /> Object <b>".get_class($this)."</b> is done...";
  }


}

 ?>
