<?php

//VIDEO 32 - Clases anonimas

echo '<style> body { margin: 2.5vw 5vw; text-align: justify; 
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Clases anonimas</h3>';
echo 'Las clases anonimas son un tipo especial de objeto que puede existir sin
tener un nombre (relativamente, ya que PHP internamente le asigna uno), y
cuya finalidad es poder aprovechar las ventajas de la POO (herencia,
polimorfismo, intefaces), sin tener que crear un objeto completo (e.g. para
delegar tareas de un objeto padre, requiriendo propiedades del mismo). <br />';
echo 'El concepto parece no estar muy adoptado y extendido en la comunidad,
como se puede apreciar en los siguientes enlaces (todos en ingles) : ';
echo '<ul>
<li><a href="https://stackoverflow.com/q/11398122">Cual es el proposito de
las clases internas (concepto similar de Java)</a></li>
<li><a href="https://stackoverflow.com/q/355167">Como son usadas las clases
anonimas internas en Java? (concepto identico en Java)</a></li>
<li><a href="https://stackoverflow.com/q/31433462">Clases anonimas en PHP 7</a></li>
</ul>';
echo 'Las clases anonimas se crean solamente colocando <i>new class</i> en
el contexto dado, y agregandole la o las propiedades, y el o los metodos que
se esperan usar en ella.<br />';
echo '<br />';
echo '<b>Nota:</b> para mas informacion, leer <a href=
"http://php.net/manual/en/language.oop5.anonymous.php">
la documentacion oficial de las clases anonimas</a>';

//VIDEO 33 - CSPRNG e intdiv

echo '<h3>CSPRNG y la funcion intdiv</h3>';
echo '<b>CSPRNG</b> significa cryptographically secure pseudo-random number generator
(generador de numeros pseudoaleatorios criptograficamente seguros), y su funcion
es la de crear numeros aleatorios encriptados para diversos usos (e.g. Tokens). <br />';
echo 'Esta es una extension añadida a PHP 7, y contiene dos funciones: <br />';
echo '<b>random_bytes(longitud) - </b> esta funcion devuelve una cantidad definida
de bytes (que, ya sea por el motor de renderizado del navegador, que convierte
el codigo binario en caracteres unicode, o la manera en que PHP devuelve este
resultado, pero no se puede visualizar adecuadamente de manera directa) que
pueden ser usados como parte de mecanismos de autenticacion y seguridad <br />';
echo '<b>random_int(minimo, maximo) - </b> esta funcion devuelve un numero entero
aleatorio que se encuentre en el rango dado (probablemente es mas seguro y tiene
menos sesgos que las funciones corrientes de generacion de aleatorios) <br />';
echo '<br />';
echo '<b>intdiv</b> es una funcion de PHP 7 que devuelve el cociente de una division
de enteros (probablemente agregada con el fin de darle mas precision a estas divisiones,
que hasta el momento se resolverian con floor). Para usarla, se la formula
<i>intdiv(dividendo,divisor)</i> <br />';
echo '<br />';
echo '<b>Nota:</b> para mas informacion, leer la documentacion oficial de
<a href="http://php.net/manual/en/book.csprng.php">CSPRNG</a>, y de
<a href="http://php.net/manual/en/function.intdiv.php">intdiv</a>';

//VIDEO 34 - Serialize/unserialize

echo '<h3>Funciones serialize y unserialize</h3>';
echo '<b>Estas funciones sirven para crear una cadena de texto que describe
en detalle la estructura del argumento que se le pase, con el fin de almacenar
esta estructura y valores en alguna capa de persistencia para su portabilidad.</b> ';
echo 'Segun comentarios de la documentacion, esta funcion es significativamente
mas ineficiente que json_encode, asi como una mala opcion si existe la posibilidad
de cambiar de lenguaje de programacion o de motor de bases de datos. <br />';
echo '<br />';
echo '<b>Como se usa serialize?</b><br />';
echo 'Utilizando la funcion <i>serialize(objeto/variable/valor)</i> se puede
crear y almacenar dicha cadena de texto portable.<br />';
echo '<b>Como se usa unserialize?</b><br />';
echo 'Utilizando la funcion <i>unserialize(cadenaSerializada)</i> se puede
recrear el o los elementos que dicha cadena representa, para su manipulacion.<br />';
echo '<b>Nota:</b>';
echo '<ul>';
echo '<li>unserialize hace posibles que vulnerabilidades criticas sean explotables
por un tercero lo suficientemente pericioso, como fue
<a href="https://www.notsosecure.com/remote-code-execution-via-php-unserialize/">
documentado por el portal NotSoSecure</a> en 2015. PHP 7 agrego como
proteccion adicional el uso de un array de opciones adicionales, con el fin de
definir aspectos como una whitelists de clases</li>';
echo '<li>para mas informacion, leer la documentacion oficial de
<a href="http://php.net/manual/en/function.serialize.php">serialize</a>, y de
<a href="http://php.net/manual/en/function.unserialize.php">unserialize</a></li>';
echo '</ul>';



 ?>
