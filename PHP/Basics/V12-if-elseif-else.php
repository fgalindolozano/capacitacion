<?php

$a=rand(0,10);
$b=rand(0,10);

function funcion($a,$b,$arg){
  if ($arg==1):
  if ($a<$b) {
    return "$a es menor que $b";
  } else {
  return "$a es mayor o igual que $b";
  }
else:
  if ($a<$b) {
    return "$a es menor que $b";
  } elseif($a==$b) {
    return "$a es igual que $b";
} else {
  return "$a es mayor que $b";
}
endif;
}

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Condicional If</h3>';
echo '<b>Valor a:</b> '.$a.' <br />';
echo '<b>Valor b:</b> '.$b.' <br />';
echo '<b>Estructura basica:</b> <br />';
echo 'if (condicion) { <br />';
echo '&emsp;true<br />';
echo '} else { <br />';
echo '&emsp;false } <br />';
echo '<b>Estructura basica simplificada:</b> <br />';
echo 'if (condicion) : <br />';
echo '&emsp;true<br />';
echo 'else : <br />';
echo '&emsp;false  <br />';
echo 'endif; <br /><br />';
echo '<b>Resultado:</b> '.funcion($a,$b,1).'<br /><br />';


echo '<b>Estructura con elseif:</b> <br />';
echo 'if (condicion) { <br />';
echo '&emsp;true<br />';
echo '} elseif(condicion2) { <br />';
echo '&emsp;true2 } <br />';
echo '} else { <br />';
echo '&emsp;false } <br />';

echo '<b>Estructura con elseif simplificada:</b> <br />';
echo 'if (condicion) : <br />';
echo '&emsp;true<br />';
echo 'elseif(condicion2) : <br />';
echo '&emsp;true2  <br />';
echo ' else : <br />';
echo '&emsp;false <br />';
echo 'endif; <br /><br />';
echo '<b>Resultado:</b> '.funcion($a,$b,'cualquiercosa').'<br /><br />';
 ?>
