<?php

function funcion(){

  $array1=[ [1,2,3],
            [4,5,6],
            [7,8,9] ];
  $array2=[ 'subarray1'=>['a','b','c'],
            'subarray2'=>['d','e','f'],
            'subarray3'=>['g','h','i'] ];

  echo '<ul>';
    for ($a=0;$a<count($array1);$a++){
      echo '<li>';
      for ($b=0;$b<count($array1[$a]);$b++){
          echo ' '.$array1[$a][$b].' ';
      }
      echo '</li>';
    }
  echo '</ul>';

  echo '<ul>';
    foreach($array2 as $key=>$val):
        echo '<li> '.$key." =>";
        foreach ($val as $v){
          echo ' '.$v.' ';
        }
    endforeach;
  echo '</ul>';
}

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Arrays multidimensionales</h3>';
echo '<b>Resultado:</b><br /><br />';
funcion();

 ?>
