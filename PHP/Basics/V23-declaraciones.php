<?php

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Declaraciones de tipo escalar</h3>';
echo 'Aunque PHP es un lenguaje debilmente tipado (esto es, no es necesario
      declarar el tipo de una variable al momento de crearla), desde PHP7 se
      tiene la flexibilidad de poder definir los tipos de los parametros de
      una funcion, asi como el tipo de variable que se espera que esa funcion
      retorne.';
echo '<b>Como declarar el tipo de variable a un parametro :</b> <br />';
echo 'function nombre (<u>(tipo)</u> arg1 = valorDefault, ..., argN) { <br />';
echo '&emsp; comportamiento;<br />';
echo '} <br />';
echo '<br />';
echo '<b>Como forzar el tipo de variable del return :</b> <br />';
echo 'function nombre (arg1 = valorDefault, ..., argN):<u>(tipo)</u> { <br />';
echo '&emsp;return <i>algo de ese tipo</i>;<br />';
echo '} <br />';
echo '<br />';
echo '<b>Como forzar el uso de los tipos de variables definidos :</b> <br />';
echo 'define (strict_types=1); <br />';
echo '<br />';
echo '<b>Nota:</b> Aunque se declaren los tipos de variable de los parametros,
      por defecto no es una obligacion respetarlos (modo coercitivo), pero
      colocando el comando anterior, PHP obligara a respetar los tipos
      declarados (siendo la unica excepcion introducir enteros en variables float)';

 ?>
