<?php

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Argumentos de Variables 2.0 + devolviendo valores</h3>';
echo '<b>Como poner valores por defecto a un parametro + retornar :</b> <br />';
echo 'function nombre (arg1 = valorDefault, ..., argN) { <br />';
echo '&emsp;return <i>algo</i>;<br />';
echo '} <br /><br />';
echo '<b>Nota:</b> return detiene totalmente la ejecucion de la funcion, y
envia de vuelta lo que se le haya alimentado (sea texto, numeros, arreglos o
variables). <br />';
echo 'Si se envio un array como return, y se desea separar su contenido
en variables, <a href="http://php.net/manual/en/function.list.php">leer la
documentacion oficial de PHP</a> para ver en detalle como funciona la
"funcion" list()<br />';

 ?>
