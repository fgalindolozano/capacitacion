<?php

$a=null;
$b=null;

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo "<h3>Operadores de asignacion</h3>";
echo '<b>Operador #1 : Asignacion (=)</b><br />';
echo 'Operacion: ($a=3) <br />';
echo 'Resultado: '.($a=3).'<br />';
echo '<b>Operador #2 : Operadores combinados (-)</b><br />';
echo 'Operacion a: ($b +=5) <br />';
echo 'Resultado a: '.($b +=5).'<br />';
echo 'Operacion b: ($a *=$b) <br />';
echo 'Resultado b: '.($a *=$b).'<br />';
echo 'Operacion c: ($a -=$b) <br />';
echo 'Resultado c: '.($a -=$b).'<br />';
echo '<b>Operador #3 : Post-incremento ($a++)</b><br />';
echo 'Operacion: ($a++) <br />';
echo 'Resultado: '.($a++).'++ = '.$a.' <br />';
echo '<b>Operador #4 : Pre-incremento (++$a)</b><br />';
echo 'Operacion: (++$a) <br />';
echo 'Resultado: '.(++$a).'<br />';
echo '<b>Operador #5 : Post-decremento ($a--)</b><br />';
echo 'Operacion: ($b--) <br />';
echo 'Resultado: '.($b--).'-- = '.$b.' <br />';
echo '<b>Operador #6 : Pre-decremento (--$a)</b><br />';
echo 'Operacion: (--$b) <br />';
echo 'Resultado: '.(--$b).'<br />';

 ?>
