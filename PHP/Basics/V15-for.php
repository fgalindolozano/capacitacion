<?php

$a=rand(0,5);

function funcion($arg){
  echo '<b>Ciclo For</b><br />';
  for ($arg;$arg<=16;$arg++):
    echo "El numero es $arg.<br />";
  endfor;
}

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Bucle For</h3>';
echo '<b>Estructura basica:</b> <br />';
echo 'for (variable; condicion; delta) { <br />';
echo '&emsp; true<br />';
echo '} <br /><br />';
echo '<b>Estructura basica "simplificada":</b> <br />';
echo 'for (variable; condicion; delta) : <br />';
echo '&emsp; true<br />';
echo 'endfor; <br /><br /><br />';
echo '<b>Resultado:</b><br /><br />';
funcion($a);

 ?>
