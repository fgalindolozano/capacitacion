<?php

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Concatenacion de cadenas</h3>';
echo '<b>Nota: </b> en PHP7 la interpolacion es mas eficiente que la concatenacion.<br /><br />';
$string='Codigo';
echo 'Cadena base: "'.$string.'" <br />';
echo '<b>Concatenacion #1</b><br />';
$union=$string." "."Facilito";
echo '$union="'.$string.'"." "."Facilito" <br />';
echo 'Cadena concatenada #1: "'.$union.'" <br />';
echo '<b>Concatenacion #2</b><br />';
$string .=" Facilito";
echo '$string .=" Facilito" <br />';
echo 'Cadena concatenada #2: "'.$string.'" <br />';
echo "<h3>Interpolacion de cadenas</h3>";
echo '<b>Nota: </b> esto requiere comillas dobles. <br /><br />';
$string='ejemplo';
echo 'Cadena base: "'.$string.'" <br />';
echo '<b>Interpolacion #1</b><br />';
echo '"esto es un $string" <br />';
echo 'Cadena interpolada #1: "'."esto es un $string".'" <br />';
echo '<b>Interpolacion #2</b> <br />';
echo '"esto es un {$string}" <br />';
echo 'Cadena interpolada #2: "'."esto es un {$string}".'" <br />';
echo '<br /><b>Notas: </b>
<br /> Es necesario usar doble comillas para usar
caracteres escapados (como saltos de linea con \n, etc.).
<br /> Es necesario usar doble slash invertido para hacer que un slash aparezca.';

 ?>
