<?php

$a=rand(0,10);
$b=rand(0,10)." miau";
$c=rand(0,1);
$d=(rand(0,100)>=50)?1:null;
//Funcion que sirve para devolver el resultado de la comparacion
function check($arg){
  if($arg){
    return "Verdadero.";
  } else {
    return "Falso.";
  }
}

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo "<h3>Operadores de comparacion</h3>";
echo '<b>Valor a:</b> '.$a.' ('.gettype($a).')<br />';
echo '<b>Valor b:</b> '.$b.' ('.gettype($b).')<br />';
echo '<b>Valor c:</b> '.check($c).' ('.gettype($c).')<br />';
echo '<b>Valor d:</b> '.check($d).' ('.gettype($d).')<br /><br />';
echo '<b>Operador #1 : Igual que (==)</b><br />';
echo 'Operacion: $a==$b <br />';
echo 'Resultado: '.check($a==$b).'<br />';
echo '<b>Operador #2 : Identico que (===)</b><br />';
echo 'Operacion: $a===$b <br />';
echo 'Resultado: '.check($a===$b).'<br />';
echo '<b>Operador #3 : No igual que (!=)</b><br />';
echo 'Operacion: $a!=$b <br />';
echo 'Resultado: '.check($a!=$b).'<br />';
echo '<b>Operador #4 : No igual que (<>)</b><br />';
echo 'Operacion: $a<>$b <br />';
echo 'Resultado: '.check($a<>$b).'<br />';
echo '<b>Operador #5 : No identico que (!==)</b><br />';
echo 'Operacion: $a!==$b <br />';
echo 'Resultado: '.check($a!==$b).'<br />';
echo '<b>Operador #6 : Mayor que (>)</b><br />';
echo 'Operacion: $a>$b <br />';
echo 'Resultado: '.check($a>$b).'<br />';
echo '<b>Operador #7 : Menor que (<)</b><br />';
echo 'Operacion: $a<$b <br />';
echo 'Resultado: '.check($a<$b).'<br />';
echo '<b>Operador #8 : Mayor o igual que (>=)</b><br />';
echo 'Operacion: $a>=$b <br />';
echo 'Resultado: '.check($a>=$b).'<br />';
echo '<b>Operador #8 : Menor o igual que (<=)</b><br />';
echo 'Operacion: $a<=$b <br />';
echo 'Resultado: '.check($a<=$b).'<br />';
echo '<b>Operador #9 : Operador nave espacial (<=>)</b><br />';
echo 'Operacion: $a<=>$b <br />';
echo 'Resultado: '.($a<=>$b).'<br />'; //Por alguna razon me da error de sintax
echo '<i><b>↑ Caso especial: </b>Este tiene 3 resultados: -1 (<), 0 (=), 1 (>).</i><br />';
echo '<b>Operador #10 : Operador Elvis (a?:b)</b><br />';
echo 'Operacion: ($c?:"False...") <br />';
echo 'Resultado: '.($c?:"False...").'<br />';
echo '<b>Operador #11 : Operador Ternario (a?b:c)</b><br />';
echo 'Operacion: ($c?check($c):check($c)."..") <br />';
echo 'Resultado: '.($c?check($c):check($c)."..").'<br />';
echo '<b>Operador #12 : Operador Fusion Null (a??b)</b><br />';
echo 'Operacion: ($d??"Not true!") <br />';
echo 'Resultado: '.($d??"Not true!").'<br />';
echo '<i><b>↑ Caso especial: </b>Este es equivalente al operador elvis,
pero requiere que la variable sea null para ejecutar b.</i><br />';

 ?>
