<?php

$integer=1;
$float=0.5;
$boolean=true;
$array=array('azul','morado','negro');
$string="Hola";

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo "<h3>Declaracion de variables</h3>";
echo '<b>Numero entero:</b> '.$integer.'<br />';
echo '<b>Numero decimal:</b> '. $float.'<br />';
if ($boolean){echo '<b>Booleano: </b>'."True".'<br />';}
echo '<b>Arreglo:</b> '.implode(", ",$array).'<br />';
echo '<b>Cadena de caracteres:</b> '.$string.'<br />';
function GlobalVars(){
  $local="LocalVar";
  echo '<b>Variable local:</b> '.$local.'<br />';
  echo '<b>Variable global:</b> '.$GLOBALS["global"].'<br />';
}
$global="GlobalVar";
GlobalVars();

 ?>
