<?php

$var="20 purr";

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Tipado de numeros enteros</h3>';
echo '<b>Contextual</b></br/>';
$int=10+$var;
echo '$int=10+"'.$var.'"<br />';
echo 'Valor: '.$int.'<br />';
echo 'Tipo: '.gettype($int).'<br />';
echo '<b>Casting / Forzado de tipo</b></br/>';
$int=(int)$var;
echo '$int=(int)"'.$var.'"<br />';
echo 'Valor: '.$int.'<br />';
echo 'Tipo: '.gettype($int).'<br />';
echo '<b>Funcion de conversion</b></br/>';
$int=intval($var);
echo '$int=intval("'.$var.'") <br />';
echo 'Valor: '.$int.'<br />';
echo 'Tipo: '.gettype($int).'<br />';
$var="5.5 bark";
echo '<h3>Tipado de numeros dobles</h3>';
echo '<b>Contextual</b></br/>';
$float=3.1416+$var;
echo '$float=3.1416+"'.$var.'"<br />';
echo 'Valor: '.$float.'<br />';
echo 'Tipo: '.gettype($float).'<br />';
echo '<b>Casting / Forzado de tipo</b></br/>';
$float=(float)$var;
echo '$float=(int)"'.$var.'"<br />';
echo 'Valor: '.$float.'<br />';
echo 'Tipo: '.gettype($float).'<br />';
echo '<b>Funcion de conversion</b></br/>';
$float=floatval($var);
echo '$float=floatval("'.$var.'") <br />';
echo 'Valor: '.$float.'<br />';
echo 'Tipo: '.gettype($float).'<br />';
$var="test";
echo '<h3>Tipado de booleanos</h3>';
echo '<b>Casting / Forzado de tipo</b></br/>';
$boolean=(bool)$var;
echo '$boolean=(boolean)"'.$var.'"<br />';
echo 'Valor: '.$boolean.'<br />';
echo 'Tipo: '.gettype($boolean).'<br />';
echo '<b>Funcion de conversion</b></br/>';
$boolean=boolval($var);
echo '$boolean=boolval("'.$var.'") <br />';
echo 'Valor: '.$boolean.'<br />';
echo 'Tipo: '.gettype($boolean).'<br />';
$var="a.b.c.d";
echo '<h3>Tipado de arreglos</h3>';
echo '<b>Casting / Forzado de tipo</b></br/>';
$array=(array)$var;
echo '$array=(boolean)"'.$var.'"<br />';
echo 'Valor: '.$array[0].'<br />';
echo 'Tipo: '.gettype($array).' ('.count($array).')<br />';
echo '<b>Funcion de conversion</b></br/>';
$array=explode(".",$var);
$arrayStringify="";
for($i=0;$i<count($array);$i++) {$arrayStringify.="[".$i."] ".$array[$i].", ";}
echo '$array=explode(".","'.$var.'") <br />';
echo 'Valor: '. substr_replace($arrayStringify, ".", -2).'<br />';
echo 'Tipo: '.gettype($array).' ('.count($array).')<br />';
$var=array('val0','val1','val2','val3');
echo '<h3>Tipado de cadenas</h3>';
echo '<b>Funcion de conversion</b></br/>';
$string=implode(", ",$var);
echo '$array=explode(".","'.$string.'") <br />';
echo 'Valor: '. $string .'<br />';
echo 'Tipo: '.gettype($string).'<br />';

 ?>
