<?php

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo "<h3>Declaracion de constantes</h3>";
echo '<b>Nota: </b>las mayusculas son solo una convencion, no una obligacion.<br /><br />';
const string1="Texto 1";
const number1=1;
const array1=array('indice0','indice1','indice2');
echo '<b>Cadena de caracteres:</b> '.string1.'<br />';
echo '<b>Numero entero:</b> '.number1.'<br />';
echo '<b>Arreglo:</b> '.implode(", ",array1).'<br />';
define("string2","Texto 2");
define("number2",2);
define("array2",array('index0','index1','index2'));
echo '<b>Cadena de caracteres:</b> '.string2.'<br />';
echo '<b>Numero entero:</b> '.number2.'<br />';
echo '<b>Arreglo:</b> '.implode(", ",array2).'<br />';

 ?>
