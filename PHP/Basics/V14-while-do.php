<?php

$a=rand(0,8);

function funcion($arg){
  $rnd=rand(0,1);
  if($rnd==0):
  echo '<b>Ciclo While</b><br />';
  while($arg > 0) {
    echo 'El numero es '.$arg.'<br />';
    $arg--;
  }
  endif;
  if($rnd==1){
  echo '<b>Ciclo Do While</b><br />';
  do{
    echo 'El numero es '.$arg.'<br />';
    $arg--;
    } while ($arg>0);
  }
}

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Bucles While/Do-While</h3>';
echo '<b>Nota:</b> La estructura switch tambien permite usar operadores logicos, comparaciones,
"rangos", etc.<br /><br />';
echo '<b>Estructura basica:</b> <br />';
echo 'switch (variable) { <br />';
echo '&emsp;case -valor1- :<br />';
echo '&emsp;&emsp;valor1 true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;&emsp;&emsp;... <br />';
echo '&emsp;case -valorN- :<br />';
echo '&emsp;&emsp;valorN true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;default : <br />';
echo '&emsp;&emsp;default true &emsp;&emsp;&emsp; <i>← Opcional, hace las veces de else</i><br />';
echo '&emsp;&emsp;break;<br />';
echo '}<br /><br />';
echo '<b>Estructura basica "simplificada":</b> <br />';
echo 'switch (variable) : <br />';
echo '&emsp;case -valor1- :<br />';
echo '&emsp;&emsp;valor1 true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;&emsp;&emsp;... <br />';
echo '&emsp;case -valorN- :<br />';
echo '&emsp;&emsp;valorN true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;default : <br />';
echo '&emsp;&emsp;default true &emsp;&emsp;&emsp; <i>← Idem</i><br />';
echo '&emsp;&emsp;break; <br />';
echo 'endswitch;<br /><br />';
echo '<b>Resultado:</b><br /><br />';
funcion($a);

 ?>
