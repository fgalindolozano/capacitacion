<?php

$a=rand(0,1);
$b=rand(0,1);
//Funcion que sirve para devolver el resultado de la comparacion
function check($arg){
  if($arg){
    return "Verdadero.";
  } else {
    return "Falso.";
  }
}

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Operadores logicos</h3>';
echo '<b>Valor a:</b> '.check($a).' <br />';
echo '<b>Valor b:</b> '.check($b).' <br /><br />';
echo '<b>Operador #1 : Y (&&)</b><br />';
echo 'Operacion: ($a && $b) <br />';
echo 'Resultado: '.check($a && $b).'<br />';
echo '<b>Operador #2 : Y (and)</b><br />';
echo 'Operacion: ($a and $b) <br />';
echo 'Resultado: '.check($a and $b).'<br />';
echo '<b>Operador #3 : O (||)</b><br />';
echo 'Operacion: ($a || $b) <br />';
echo 'Resultado: '.check($a || $b).'<br />';
echo '<b>Operador #4 : O (or)</b><br />';
echo 'Operacion: ($a or $b) <br />';
echo 'Resultado: '.check($a or $b).'<br />';
echo '<b>Operador #5 : XOR (xor)</b><br />';
echo 'Operacion: ($a || $b) <br />';
echo 'Resultado: '.check($a xor $b).'<br />';
echo '<b>Operador #6 : NOT (!)</b><br />';
echo 'Operacion: !($a) <br />';
echo 'Resultado: '.check(!$a).'<br /><br />';

echo '<b>Nota:</b> <a href="http://php.net/manual/en/language.operators.precedence.php">
Clic para mas informacion de la prioridad de los operadores</a>';
 ?>
