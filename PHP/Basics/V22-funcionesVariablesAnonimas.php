<?php

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Funciones anonimas, funciones variables y comprobar existencia
      de funciones</h3>';
echo '<b>Estructura de definicion de las funciones anonimas :</b> <br />';
echo 'variable = function(arg1 = valorDefault, ..., argN) { <br />';
echo '&emsp;comportamiento;<br />';
echo '}; <br />';
echo '<b>Estructura de llamado de las funciones anonimas :</b> <br />';
echo 'variable(arg1 = valorDefault, ..., argN); <br />';
echo '<br />';
echo '<b>Estructura de definicion de las funciones variables :</b> <br />';
echo 'Igual a una variable normal<br />';
echo '<b>Estructura de llamado de las funciones variables :</b> <br />';
echo 'variable="nombreFuncion"<br />';
echo 'variable(arg1 = valorDefault, ..., argN); <br />';
echo '<br />';
echo '<b>Como comprobar la existencia de una funcion :</b> <br />';
echo 'function_exists("nombreFuncion")<br />';
echo '<b>Nota:</b> Esta funcion <a href="http://php.net/manual/en/function.function-exists.php">
      devuelve como resultado un booleano.</a> <br />';

 ?>
