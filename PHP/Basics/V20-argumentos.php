<?php

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Argumentos de Variables</h3>';
echo '<b>Estructura basica:</b> <br />';
echo 'function nombre (arg1, ..., argN) { <br />';
echo '&emsp;comportamiento<br />';
echo '} <br /><br />';
echo '<b>Nota:</b> Aunque los parametros crean una variable espejo si se les pasa
 una variable, las alteraciones que sufran dentro de una funcion se pueden
 anclar usando el signo ampersand (&) detras del nombre de las mismas, al
 momento de pasarlas como argumento. Ademas, las funciones de PHP pueden ser
 usadas con recursividad (osease, llamarse a si misma en su estructura).<br /><br />';

 ?>
