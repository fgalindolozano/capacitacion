<?php

echo '<style>
body { margin: 2.5vw 5vw; text-align: justify; font-family: Tahoma, Geneva, sans-serif }
table { width:25%; }
table th { background:#c0c0c0; }
table, th, td { border: 1px solid black; border-collapse: collapse; }
td b, td p { display: block; text-align: center; }
</style>';

function funcion(){

  $array=array();
  foreach(array(rand(0,10),rand(0,10),rand(0,10),rand(0,10),rand(0,10)) as $key=>$val){
    $array[$key]=$val*rand(2,4);
  }
  $a=0;
  echo '
  <table>
  <tr>
  <th>Index</th>
  <th>Value</th>
  </th>';
  foreach($array as $val):
    $a++;
    echo "<tr>
    <td><b>$a</b></td>
    <td><p>$val</p></td>
    </tr>";
  endforeach;
  echo '</table>';
}

echo '<h3>Bucle ForEAch</h3>';
echo '<b>Estructura basica:</b> <br />';
echo 'foreach (arreglo/objeto as val) { <br />';
echo '&emsp; true<br />';
echo '} <br /><br />';
echo '<b>Estructura basica "simplificada":</b> <br />';
echo 'foreach (arreglo/objeto as val) : <br />';
echo '&emsp; true<br />';
echo 'endforeach; <br /><br />';
echo '<b>Estructura completa:</b> <br />';
echo 'foreach (arreglo/objeto as key=>val) { <br />';
echo '&emsp; true<br />';
echo '} <br /><br />';
echo '<b>Estructura completa "simplificada":</b> <br />';
echo 'foreach (arreglo/objeto as key=>val) : <br />';
echo '&emsp; true<br />';
echo 'endforeach; <br /><br />';
echo '<b>Resultado:</b><br /><br />';
funcion();

 ?>
