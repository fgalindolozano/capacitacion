<?php

$a=rand(-10,10);

function funcion($arg){
  switch ($arg) {
    case -1: case -5: case -10: {
      return "multiples cases: el numero es -1, -5 o -10";
      break;
    }
  case ($arg>0 && $arg<=5) : {
      return "operadores: el numero se encuentra entre 1 y 5";
      break;
    }
default: {
      return "default: el numero es {$arg}";
    }
  }
}

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Condicional Switch</h3>';
echo '<b>Valor a:</b> '.$a.' <br /><br />';
echo '<b>Estructura basica:</b> <br />';
echo 'switch (variable) { <br />';
echo '&emsp;case -valor1- :<br />';
echo '&emsp;&emsp;valor1 true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;&emsp;&emsp;... <br />';
echo '&emsp;case -valorN- :<br />';
echo '&emsp;&emsp;valorN true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;default : <br />';
echo '&emsp;&emsp;default true &emsp;&emsp;&emsp; <i>← Opcional, hace las veces de else</i><br />';
echo '&emsp;&emsp;break;<br />';
echo '}<br /><br />';
echo '<b>Estructura basica "simplificada":</b> <br />';
echo 'switch (variable) : <br />';
echo '&emsp;case -valor1- :<br />';
echo '&emsp;&emsp;valor1 true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;&emsp;&emsp;... <br />';
echo '&emsp;case -valorN- :<br />';
echo '&emsp;&emsp;valorN true <br />';
echo '&emsp;&emsp;break; <br />';
echo '&emsp;default : <br />';
echo '&emsp;&emsp;default true &emsp;&emsp;&emsp; <i>← Idem</i><br />';
echo '&emsp;&emsp;break; <br />';
echo 'endswitch;<br /><br />';
echo '<b>Resultado:</b> '.funcion($a).'<br /><br />';
echo '<b>Nota:</b> La estructura switch, como se puede ver en este archivo, permite usar
operadores logicos, comparaciones, "rangos", expresiones compuestas, etc.';

 ?>
