<?php

$a=rand(-25,25);
$b=rand(1,25)." miau";

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo "<h3>Operadores aritmeticos</h3>";
echo '<b>Valor a:</b> '.$a.' ('.gettype($a).')<br />';
echo '<b>Valor b:</b> '.$b.' ('.gettype($b).')<br /><br />';
echo '<b>Operador #1 : Suma (+)</b><br />';
echo 'Operacion: ($a+$b) <br />';
echo 'Resultado: '.($a+$b).'<br />';
echo '<b>Operador #2 : Resta (-)</b><br />';
echo 'Operacion: ($a-$b) <br />';
echo 'Resultado: '.($a-$b).'<br />';
echo '<b>Operador #3 : Multiplicacion (*)</b><br />';
echo 'Operacion: ($a*$b) <br />';
echo 'Resultado: '.($a*$b).'<br />';
echo '<b>Operador #4 : Division (/)</b><br />';
echo 'Operacion: ($a/$b) <br />';
echo 'Resultado: '.($a/$b).'<br />';
echo '<b>Operador #5 : Modulo (%)</b><br />';
echo 'Operacion: ($a%$b) <br />';
echo 'Resultado: '.($a%$b).'<br />';
echo '<b>Operador #6 : Potenciacion (**)</b><br />';
echo 'Operacion: ($a**$b) <br />';
echo 'Resultado: '.($a**$b).'<br />';
echo '<b>Operador #7 : Identidad (+a)</b><br />';
echo 'Operacion: (+$a) <br />';
echo 'Resultado: '.(+$a).'<br />';
echo '<b>Operador #8 : Negacion (-a)</b><br />';
echo 'Operacion: (-$a) <br />';
echo 'Resultado: '.(-$a).'<br />';

 ?>
