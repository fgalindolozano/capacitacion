<?php

echo '<style> body { margin: 2.5vw 5vw; text-align: justify;
      font-family: Tahoma, Geneva, sans-serif } </style>';
echo '<h3>Namespaces</h3>';
echo 'Los namespaces son una manera de crear contenedores virtuales para el
codigo PHP que usemos, con el fin de evitar conflictos (situacion que se
puede dar facilmente al usar uno o mas frameworks, librerias o plugins).
<br />';
echo '<br />';
echo '<b>Como crear un namespace?</b> <br />';
echo '&emsp;namespace {nombre}\{nombre};<br /><br />';
echo '<b>Nota:</b> Como buena practica, se deberia colocar al inicio del
fichero, y deberia componerse de la ruta donde se encuentre el fichero. <br />
Tambien, un mismo fichero puede tener varios namespaces, en ese caso, es
recomendable encerrar en llaves todo su contenido.<br />
Ademas, si el fichero contiene una clase que hereda de otra, se debe
colocar backlash antes de su nombre (i.e. extends \<i>padre</i>).<br />
El backlash en la clase padre se debe omitir en caso de que dicha clase padre
este contenida dentro de un namespace, y en esos casos se debe llamar apropiadamente. <br />
Al momento de llamar un clase/funcion que se encuentra dentro de namespaces,
se debe enunciar su ruta virtual exacta (osease, su namespace y su nombre),
pero existe un workaround: <br />';
echo '<br />';
echo '<b>Como definir la ruta virtual para uno o varios elementos contenidos en namespaces?</b><br />';
echo '&emsp;use {nombre}\{nombre}\{clase/funcion} as {alias};<br />';
echo '<b>Nota:</b> Si existen varios elementos con una parte de la ruta en comun
(e.g. clases/A y clases/B), se puede abrir llaves y colocarlos dentro de una sola
declaracion, separandolos por comas. <br />';
echo '<br />';
echo '<br /><b>Nota:</b> <a href="http://php.net/manual/en/language.namespaces.php">
Clic aqui para mas leer mas acerca de los namespaces.</a><br />';

 ?>
